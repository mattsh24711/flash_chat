import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

User getCurrentUser(FirebaseAuth _auth) {
  User loggedInUser;
  final user = _auth.currentUser;
  loggedInUser = user!;
  return loggedInUser;
}

Future<List<String>> getUsersList(
    FirebaseFirestore _firestore, List<String> userDirectory) async {
  userDirectory.add('No users found to add:(');
  Future.delayed(const Duration(milliseconds: 500));
  await _firestore.collection("users").get().then((querySnapshot) {
    querySnapshot.docs.forEach((value) {
      userDirectory.add(value.data()['email']);
      userDirectory.add(value.data()['id']);
    });
  }).catchError((onError) {
    print("getCloudFirestoreUsers: ERROR");
    print(onError);
  });
  return userDirectory;
}
