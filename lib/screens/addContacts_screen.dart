import 'package:flash_chat/screens/chat_screen.dart';
import 'package:flash_chat/utils/auth_utils.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final _firestore = FirebaseFirestore.instance;

class contactsScreenArgs {
  final List<String> usersList;
  final String userPrivateKey;
  final String userId;
  contactsScreenArgs(this.usersList, this.userPrivateKey, this.userId);
}

class addContactsScreen extends StatelessWidget {
  static String name = 'addContactsScreen';

  @override
  Widget build(BuildContext context) {
    final _auth = FirebaseAuth.instance;
    final User currentUser = getCurrentUser(_auth);
    final args =
        ModalRoute.of(context)!.settings.arguments as contactsScreenArgs;
    List<Widget> users = [];

    if (args.usersList.length < 4) {
      users.add(Padding(
        padding: const EdgeInsets.all(30.0),
        child: Text(
          'No users found to add ;(',
          style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
        ),
      ));
    } else {
      args.usersList.removeAt(0);

      for (var i = 1; i < args.usersList.length; i = i + 2) {
        if (args.usersList[i] == args.userId) {
          args.usersList.removeAt(i - 1);
          args.usersList.remove(currentUser.uid);
        }
      }

      for (var i = 0; i < args.usersList.length; i = i + 2) {
        users.add(MessageBubble(
            email: args.usersList[i],
            userID: args.userId,
            userPrivateKey: args.userPrivateKey,
            friendID: args.usersList[i + 1]));
      }
    }
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
              }),
        ],
        title: Text('⚡️Select a friend to chat with'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: users,
          ),
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble(
      {required this.email,
      required this.userID,
      required this.userPrivateKey,
      required this.friendID});

  late final String email;
  late final String userID;
  late final String friendID;
  late final String userPrivateKey;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () async {
              final List<String> IDs = ['$userID', '$friendID'];
              IDs.sort();
              final String convoID = "${IDs[0]}${IDs[1]}";

              _firestore.collection('conversations').doc(convoID).set({
                'lastMessage': 'You can start by saying hello! :)',
                'senderID': userID,
                'recieverID': friendID,
                'randomMsgKey': 'randomKey',
              });
              var snapshot = await _firestore
                  .collection('users')
                  .doc(friendID)
                  .snapshots()
                  .first;
              final String publicKey = snapshot.data()!['publicKey'];
              Navigator.pushNamed(context, ChatScreen.name,
                  arguments: chatScreenArgs(
                      userPrivateKey, userID, friendID, convoID, publicKey));
            },
            child: Text(
              '$email',
              style: TextStyle(
                color: Colors.white,
                fontSize: 15.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
