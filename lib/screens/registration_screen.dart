import 'package:flash_chat/screens/addContacts_screen.dart';
import 'package:flash_chat/screens/chat_screen.dart';
import 'package:flash_chat/utils/auth_utils.dart';
import 'package:flutter/material.dart';
import 'package:flash_chat/components/rounded_button.dart';
import 'package:flash_chat/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_chat/utils/security_utils.dart';
import 'package:crypton/crypton.dart';

class RegistrationScreen extends StatefulWidget {
  static String name = 'Registration Screen';
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _firestore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  late String email;
  late String password;
  bool _showSpinner = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ModalProgressHUD(
        inAsyncCall: _showSpinner,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Flexible(
                child: Hero(
                  tag: 'logo',
                  child: Container(
                    height: 200.0,
                    child: Image.asset('images/logo.png'),
                  ),
                ),
              ),
              SizedBox(
                height: 48.0,
              ),
              TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your email')),
              SizedBox(
                height: 8.0,
              ),
              TextField(
                  obscureText: true,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    password = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your password')),
              SizedBox(
                height: 24.0,
              ),
              RoundedButton(
                title: 'Register',
                color: Colors.blueAccent,
                onPressed: () async {
                  setState(() {
                    _showSpinner = true;
                  });
                  final String encryptionKey = Argon2(password, true);
                  final String hashedPassword = Argon2(password, false);

                  RSAKeypair rsaKeypair = RSAKeypair.fromRandom();
                  print(
                      'private key: ${rsaKeypair.privateKey.toFormattedPEM()}');
                  try {
                    final newUser = await _auth.createUserWithEmailAndPassword(
                        email: email, password: hashedPassword);
                    _firestore.collection('users').doc(newUser.user!.uid).set({
                      'id': newUser.user!.uid,
                      'email': email,
                      'publicKey': rsaKeypair.publicKey.toPEM(),
                      'encPrivateKey': keyAES(
                          rsaKeypair.privateKey.toFormattedPEM(),
                          encryptionKey,
                          true),
                    });
                    List<String> userDirectory = [];
                    getUsersList(_firestore, userDirectory);
                    print(userDirectory);
                    Navigator.pushNamed(context, addContactsScreen.name,
                        arguments: contactsScreenArgs(
                            userDirectory,
                            rsaKeypair.privateKey.toFormattedPEM(),
                            newUser.user!.uid));
                    setState(() {
                      _showSpinner = false;
                    });
                  } catch (e) {
                    print(e);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
