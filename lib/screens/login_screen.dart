import 'package:flash_chat/constants.dart';
import 'package:flash_chat/utils/auth_utils.dart';
import 'package:flutter/material.dart';
import 'package:flash_chat/components/rounded_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:flash_chat/utils/security_utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'addContacts_screen.dart';

class LoginScreen extends StatefulWidget {
  static String name = 'Login Screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _firestore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  late String email;
  late String password;
  bool _showSpinner = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ModalProgressHUD(
        inAsyncCall: _showSpinner,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Flexible(
                child: Hero(
                  tag: 'logo',
                  child: Container(
                    height: 200.0,
                    child: Image.asset('images/logo.png'),
                  ),
                ),
              ),
              SizedBox(
                height: 48.0,
              ),
              TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your email')),
              SizedBox(
                height: 8.0,
              ),
              TextField(
                  obscureText: true,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    password = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your password')),
              SizedBox(
                height: 24.0,
              ),
              RoundedButton(
                title: 'Log In',
                color: Colors.lightBlueAccent,
                onPressed: () async {
                  setState(() {
                    _showSpinner = true;
                  });
                  var key = Argon2(password, true);
                  password = Argon2(password, false);
                  // String s =
                  //     stringAES('This is not my password', password, true);
                  // print("result of decryption - loginScreen: ");
                  // var res = stringAES(s, password, false);
                  // print("final result: $res");

                  try {
                    final user = await _auth.signInWithEmailAndPassword(
                        email: email, password: password);
                    var snapshot = await _firestore
                        .collection('users')
                        .doc(_auth.currentUser?.uid)
                        .snapshots()
                        .first;
                    final String userID = snapshot.data()!['id'];
                    final String encryptedPrivateKey =
                        snapshot.data()!['encPrivateKey'];
                    var privateKey = keyAES(encryptedPrivateKey, key, false);
                    List<String> userDirectory = [];
                    await getUsersList(_firestore, userDirectory);
                    print(userDirectory);
                    Navigator.pushNamed(context, addContactsScreen.name,
                        arguments: contactsScreenArgs(
                            userDirectory, privateKey, userID));

                    setState(() {
                      _showSpinner = false;
                    });
                  } catch (e) {
                    print(e);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
