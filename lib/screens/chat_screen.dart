import 'package:crypton/crypton.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flash_chat/utils/security_utils.dart';
import 'package:flutter/material.dart';
import 'package:flash_chat/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_chat/utils/auth_utils.dart';
import 'package:firebase_auth/firebase_auth.dart';

final _firestore = FirebaseFirestore.instance;

class chatScreenArgs {
  final String userPrivateKey;
  final String userId;
  final String friendID;
  final String convoID;
  final String friendPublicKey;
  chatScreenArgs(this.userPrivateKey, this.userId, this.friendID, this.convoID,
      this.friendPublicKey);
}

class ChatScreen extends StatefulWidget {
  static String name = 'Chat Screen';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();
  final _auth = FirebaseAuth.instance;
  late User loggedInUser;
  late String messageText;

  // void messagesStream() async {
  //   await for (var snapshot in _firestore.collection('messages').snapshots()) {
  //     for (var message in snapshot.docs) {
  //       print(message.data());
  //     }
  //   }
  // }

  @override
  void initState() {
    super.initState();
    loggedInUser = getCurrentUser(_auth);
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as chatScreenArgs;

    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
              }),
        ],
        title: Text('⚡️Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MessagesStream(loggedInUser, args.convoID, args.userPrivateKey,
                args.friendPublicKey),
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: messageTextController,
                      onChanged: (value) {
                        messageText = value;
                      },
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      messageTextController.clear();
                      String randomMsgKey = SecureRandom(16).base16;
                      List<String> keyMsgPair = messageAES(messageText,
                          args.friendPublicKey, true, randomMsgKey, false);
                      _firestore
                          .collection('conversations')
                          .doc(args.convoID)
                          .collection('messages')
                          .add({
                        'text': keyMsgPair[0],
                        'sender': args.userId,
                        'reciever': args.friendID,
                        'randomMsgKey': keyMsgPair[1],
                      });
                    },
                    child: Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessagesStream extends StatelessWidget {
  MessagesStream(this.loggedInUser, this.convoID, this.userPrivateKey,
      this.friendPublicKey);
  final String convoID;
  final User loggedInUser;
  final String userPrivateKey;
  final String friendPublicKey;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firestore
          .collection('conversations')
          .doc(convoID)
          .collection('messages')
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        final messages = snapshot.data!.docs.reversed;
        print('Length of messages: ${messages.length}');
        List<MessageBubble> messageBubbles = [];
        for (var message in messages) {
          final messageText = (message.data() as dynamic)['text'];
          final messageSender = (message.data() as dynamic)['sender'];
          final currentUser = loggedInUser.uid;
          final messageReceiver = (message.data() as dynamic)['reciever'];
          final messageKey = (message.data() as dynamic)['randomMsgKey'];

          final bool isMe = currentUser == messageSender;
          final messageBubble = MessageBubble(
            sender: messageSender,
            text: isMe
                ? messageText
                // ? messageAES(
                //     messageText, userPrivateKey, false, messageKey, true)[0]
                : messageAES(
                    messageText, userPrivateKey, false, messageKey, true)[0],
            isMe: isMe,
          );
          messageBubbles.add(messageBubble);
        }
        return Expanded(
          child: ListView(
            reverse: true,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            children: messageBubbles,
          ),
        );
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble({required this.sender, required this.text, required this.isMe});

  late final String sender;
  late final String text;
  late final bool isMe;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end,
        children: [
          Text(
            sender,
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.black54,
            ),
          ),
          Material(
            borderRadius: isMe
                ? BorderRadius.only(
                    topRight: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                    bottomRight: Radius.circular(30.0),
                  )
                : BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                    bottomRight: Radius.circular(30.0),
                  ),
            elevation: 5.0,
            color: isMe ? Colors.white : Colors.lightBlueAccent,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text(
                '$text',
                style: TextStyle(
                  color: isMe ? Colors.black54 : Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
