import 'package:flash_chat/screens/addContacts_screen.dart';
import 'package:flash_chat/screens/contacts_screen.dart';
import 'package:flutter/material.dart';
import 'package:flash_chat/screens/welcome_screen.dart';
import 'package:flash_chat/screens/login_screen.dart';
import 'package:flash_chat/screens/registration_screen.dart';
import 'package:flash_chat/screens/chat_screen.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(FlashChat());
}

class FlashChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: WelcomeScreen(),
      routes: {
        // (input) => output; is an anonymous function.
        WelcomeScreen.name: (context) => WelcomeScreen(),
        ChatScreen.name: (context) => ChatScreen(),
        LoginScreen.name: (context) => LoginScreen(),
        RegistrationScreen.name: (context) => RegistrationScreen(),
        ContactsScreen.name: (context) => ContactsScreen(),
        addContactsScreen.name: (context) => addContactsScreen(),
      },
      //initialRoute: WelcomeScreen.name,
      initialRoute: WelcomeScreen.name,
    );
  }
}
